import * as THREE from 'three'

class Floor {

    constructor(options) {

        this.object = new THREE.Group();

        this.initMaterial();
        this.init();
        this.time = 0.04;
        this.value = 0;


    }

    init() {



    var glsl = require('glslify')
        //
    this.vertex_shader = glsl.file("../shaders/texture.vert")
    this.fragment_shader = glsl.file("../shaders/texture.frag")

    this.texture = STORAGE.assets.moon;
    this.texture.needsUpdate = true;
    this.texture.wrapS = THREE.RepeatWrapping;
    this.texture.wrapT = THREE.RepeatWrapping;
    this.texture.repeat.set( 140, 140 );

    this.noise = STORAGE.assets.noise;
    this.noise.wrapS = THREE.RepeatWrapping;
    this.noise.wrapT = THREE.RepeatWrapping;
    this.texture.repeat.set( 1, 1 );

    this.road = STORAGE.assets.road;
    this.road.repeat.set(1, 20);
    this.road.wrapT = THREE.RepeatWrapping;
    this.road.wrapS = THREE.RepeatWrapping;

    var geometry = new THREE.PlaneGeometry(2048, 2048, 512, 512);

    var phongShader = THREE.ShaderLib['phong'];

    var uniforms = {};
    Object.assign(uniforms, phongShader.uniforms, {
                          time: {type :'f', value: 0.},
                          height: { type: 'f', value: 50. },
                          noise_map: { type: 't', value: this.noise}
                        } )

    this.materialShader = new THREE.ShaderMaterial({
    uniforms: uniforms,
    vertexShader: this.vertex_shader,
    fragmentShader: this.fragment_shader,
    lights: true,
})

    this.materialShader.uniforms.map.value = this.texture;
    this.materialShader.uniforms.diffuse.value = new THREE.Color( 0x954600 );


    var plane = new THREE.Mesh(geometry, this.materialShader );

    plane.rotation.x = - Math.PI / 2;
    plane.position.y = - 1.8;


    this.object.add(plane);

    var roadGeo = new THREE.PlaneBufferGeometry(15, 512, 256, 256);


    this.roadMaterial = new THREE.MeshPhongMaterial({
        side : THREE.DoubleSide,
        lights: true,
        map: 0xffffff,
        map: this.road
    });

    var road = new THREE.Mesh(roadGeo, this.roadMaterial );
    road.rotation.x = - Math.PI / 2;
    road.position.y = - 1;
    this.object.add(road);


    }

    initMaterial(){



    }

    update(){


          if(STORAGE.vitess) this.time = STORAGE.vitess;

          this.materialShader.uniforms.time.value += this.time;
          this.roadMaterial.map.offset.y += this.time;


    }
}

export default Floor
