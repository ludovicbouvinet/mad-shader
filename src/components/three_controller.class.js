
import * as TOOLS from './tools.class.js'
import * as THREE from 'three'
import {TweenMax, Power2, TimelineLite} from "gsap";

import MTLLoader from '../libs/MTLLoader.js'
import OBJLoader from '../libs/ObjLoader.js'
import VRControls from '../libs/VRControls.js'
import VREffect from '../libs/VREffect.js'
import SkyL from '../libs/Sky.js'

import Floor from './floor.class.js'
import Car from './car.class.js'
import Sky from './sky.class.js'

class THREE_Controller {

    constructor(options) {

        this.options            = options
        this.container          = this.options.container
        this.debug              = this.options.debug || false
        this.width              = this.container.offsetWidth
        this.height             = this.container.offsetHeight
        this.camera             = new Object()
        this.assets             = new Object()
        this.scene              = new THREE.Scene()
        this.mouse              = new THREE.Vector2(0, 0)
        this.direction          = new THREE.Vector2(0, 0)
        this.cameraPosition     = new THREE.Vector2(0, 0)
        this.cameraEasing       = {x: 100, y: 10}
        this.time               = 0
        this.readyToUpdate = false;

        this.interface = {
            begin : document.getElementsByClassName("begin"),
            complete : document.getElementsByClassName("loader"),
            back : document.getElementsByClassName("bblack"),
            loading : document.getElementsByClassName("onLoad")
        };


        this.init_loader()
        this.init_environement()
        this.init_camera()
        this.init_event()
        this.init_lights()
        this.init_material()
        this.init_sounds()
        this.init_object()


    }

    init_loader() {

       this.objectsToLoad = {
            nbr : 0,
            total : 2
       }


       STORAGE.objectsToLoad = this.objectsToLoad;

       this.assets.moon = new THREE.TextureLoader(this.manager).load( "textures/moon.png" );
       this.assets.noise = new THREE.TextureLoader(this.manager).load( "textures/noise.jpg" );
       this.assets.sand = new THREE.TextureLoader(this.manager).load( "textures/texture.jpg" );
       this.assets.road = new THREE.TextureLoader(this.manager).load( "textures/road.jpg" );


       STORAGE.assets = this.assets;



        this.managerEnd = () => {

             STORAGE.objectsToLoad.nbr += 1;

             STORAGE.loading = ( STORAGE.objectsToLoad.nbr / STORAGE.objectsToLoad.total ) * 100;

             this.interface.loading[0].innerHTML = STORAGE.loading + '%';

             if (STORAGE.loading == 100) {

                var tween = TweenMax.fromTo(this.interface.loading[0], 0.5, {opacity: 1},{y: '110%', opacity: 0, display: 'none', onComplete: () => {
                                var tween = TweenMax.fromTo(this.interface.begin, 0.5, {y: '110%', opacity: 0},{opacity: 1, display: 'inline-block'});
                                tween.play();
                }});

                tween.play();

             }


        }

       STORAGE.managerEnd = this.managerEnd;

       var mtlLoader = new THREE.MTLLoader();
       mtlLoader.setPath( 'dodge/' );
       mtlLoader.load( 'CHALLENGER71.mtl', ( materials ) => {
            materials.preload();
            var objLoader = new THREE.OBJLoader();
            objLoader.setMaterials( materials );
            objLoader.setPath( 'dodge/' );
            objLoader.load( 'CHALLENGER71.obj',  ( object ) => {
                object.position.z = 0;
                object.position.x = 0.4;
                object.position.y = -1.17;
                object.rotation.z = Math.PI;
                object.rotation.x = - Math.PI/2;
                this.assets.car = object;

                this.car = new Car;
                this.scene.add(this.car.object);

                this.managerEnd()

            } );
       });


    }

    init_camera() {

        this.camera = new THREE.PerspectiveCamera( 55, window.innerWidth/window.innerHeight, 0.1, 2000000  );

        this.controls = new THREE.VRControls( this.camera );

		this.scene.fog = new THREE.Fog( 0x000000, 10, 100 );

        this.effect.render( this.scene, this.camera );


    }

    init_environement() {

        this.renderer = new THREE.WebGLRenderer({
            antialias: true,
            alpha: true
        });
        this.renderer.setPixelRatio(window.devicePixelRatio)
        this.renderer.setSize(this.width, this.height)

        this.vrDisplay;

        navigator.getVRDisplays().then((displays) => {

            if (displays.length > 0) {
                this.vrDisplay = displays[0];
            }

            if(!displays[0].capabilities.canPresent){

                let vrButton = document.querySelector('#startVR');

                vrButton.removeAttribute("class");
                TweenMax.set(vrButton, {display: 'none'});


            }
        });

        document.querySelector('#startVR').addEventListener('click', () => {
            this.vrDisplay.requestPresent([{source: this.renderer.domElement}]);
        });



        this.effect = new THREE.VREffect(this.renderer);
        this.effect.setSize(window.innerWidth, window.innerHeight);
        this.container.appendChild(this.renderer.domElement)
    }

    init_event() {
        var that = this
        window.addEventListener('resize', function() {
            that.width = window.innerWidth
            that.height = window.innerHeight
            that.camera.aspect = that.width / that.height;
            that.camera.updateProjectionMatrix();
            that.renderer.setSize(that.width, that.height);
        }, false)

        window.addEventListener("mousemove", function(event) {
            that.mouse.x = (event.clientX / that.width - .5) * 2
            that.mouse.y = (event.clientY / that.height - .5) * 2
        })

        for(let i = 0; i < that.interface.begin.length; i ++){

            that.interface.begin[i].addEventListener('click', () => {

                var tween = TweenMax.to(this.interface.complete, 1, {y: '-50px', opacity: 0});

                var tween2 = TweenMax.fromTo(this.interface.back, 1, {opacity: 1},{opacity: 0, display: 'none'});

                tween.play();
                tween2.play();

                this.readyToUpdate = true;

            })

        }

        document.addEventListener("keydown",(e) => {

           if(e.keyCode === 65 && this.readyToUpdate == true  ){

               var canvas = document.getElementsByTagName('canvas');
               var elem = canvas[0];
               if (elem.requestFullscreen) {
                 elem.requestFullscreen();
               } else if (elem.mozRequestFullScreen) {
                 elem.mozRequestFullScreen();
               } else if (elem.webkitRequestFullscreen) {
                 elem.webkitRequestFullscreen();
               }

           }

        });

    }

    init_sounds(){

    //Create an AudioListener and add it to the camera
    this.listener = new THREE.AudioListener();
    this.camera.add( this.listener );

    this.audioLoader = new THREE.AudioLoader();

    STORAGE.listener = this.listener;
    STORAGE.audioLoader = this.audioLoader;

    }

    init_lights(){

        var globalLight = new THREE.AmbientLight( 0x404040, .6 ); // soft white light
        this.scene.add( globalLight );


    }

    init_material(){


    }

    init_object(){


           this.floor = new Floor;
           this.scene.add(this.floor.object);

           this.sky = new Sky;
           this.scene.add(this.sky.object);


    }



    update() {


        if(this.readyToUpdate == true){

                this.controls.update();

                if ( this.floor != undefined ) { this.floor.update() }
                if ( this.car != undefined ) { this.car.update() }
                if ( this.sky != undefined ) { this.sky.update() }
                var randomY = Math.random() * (0.001 + 0.001) - 0.001;
                this.camera.position.y = 0.15 + randomY;



        }else {

                this.camera.position.y = 0.15;
        }

            this.camera.position.z = -0.8;
            this.camera.position.x = 1.1;

            this.effect.render( this.scene, this.camera );


    }


}

export default THREE_Controller