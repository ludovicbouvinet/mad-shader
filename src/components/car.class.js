import * as THREE from 'three'

class Car {

    constructor(options) {

        this.object = new THREE.Group();

        this.initMaterial();
        this.init();
        this.initLight();
        this.initSounds();
        this.time = 0;
        this.value = 0;


    }

    init() {


        this.car = STORAGE.assets.car;

        this.object.add(this.car);

        this.car.position.x = 1.5;
        this.car.position.z = -1;
        this.car.position.y = -1;


        this.vitess = {
            value: 0.06,
            min: 0.06,
            max: 0.08,
            state: 'none'
        };

        this.axe = {
            value: this.car.rotation.x,
            min: this.car.rotation.x,
            max: this.car.rotation.x + Math.PI/60
        };

        var vitessUpdateFinished = () => {

            if(this.vitess.state == 'down'){
                this.ambiantDownStill.play();
            }
            this.vitess.state = 'none';
        }

        var TopFinished = () => {
            var rotate = TweenMax.fromTo(this.axe, 1, {value: this.axe.value},{value: this.axe.max - Math.PI/300});
            rotate.play();
            this.ambiantUpStill.play();
        }


        this.musicsPlayables = [];
        this.musicsLoaded = 0;


        document.addEventListener("keydown", (e) => {


            if(e.keyCode === 32 && this.vitess.state == 'none' ){

                if(this.vitess.value < (this.vitess.min + 0.01)){
                    this.vitess.state = 'up';
                    var tween = TweenMax.fromTo(this.vitess, 2, {value: this.vitess.value},{value: this.vitess.max, onComplete: vitessUpdateFinished});
                    var rotate = TweenMax.fromTo(this.axe, 2, {value: this.axe.value},{value: this.axe.max, onComplete: TopFinished});
                    tween.play();
                    rotate.play();

                    this.ambiantDownStill.pause();
                    this.ambiantUp.play();

                }else if(this.vitess.value > (this.vitess.max - 0.1)) {
                    this.vitess.state = 'down';
                    var tween = TweenMax.fromTo(this.vitess, 1, {value: this.vitess.value},{value: this.vitess.min, onComplete: vitessUpdateFinished});
                    var rotate = TweenMax.fromTo(this.axe, 1, {value: this.axe.value},{value: this.axe.min});
                    tween.play();
                    rotate.play();

                    this.ambiantUpStill.pause();
                    this.ambiantDown.play();
                }

            }else if(e.keyCode === 75){

                if(this.ligthOpen == true){

                    this.spotLight1.intensity = 0;
                    this.spotLight2.intensity = 0;
                    this.ligthOpen = false;

                }else if(this.ligthOpen == false){

                    this.spotLight1.intensity = 3;
                    this.spotLight2.intensity = 3;
                    this.ligthOpen = true;

                }

            }else if(e.keyCode == 77 && this.musicsLoaded > 2){

                let numberMemory;
                let newValue;

                for(let i = 0; i < this.musicsLoaded; i++){


                    if(this.musicsPlayables[i].isPlaying){

                        this.musicsPlayables[i].pause();
                        numberMemory = i;

                    }

                }

                this.musicsPlayables[0].play();

                do{

                    newValue = Math.floor((Math.random() * this.musicsLoaded));

                }while(numberMemory == newValue);

                setTimeout(() => {

                    this.musicsPlayables[0].pause();
                    this.musicsPlayables[newValue].play();

                }, 2000);


            }







        });

    }


    initLight(){

    this.light = new THREE.PointLight( 0xffffff, 0.1, 100 );
    this.light.position.set( 1.3, 0.15,  -0.5 );
    this.object.add( this.light );

       /// PHARE 1 ////


       this.ligthOpen = true;

       var geometry = new THREE.SphereGeometry(0.001);

       var material = new THREE.MeshBasicMaterial({
           color: 0xffffff,
           side : THREE.DoubleSide,
           transparent: true
       });

       var point1 = new THREE.Mesh(geometry, material);

       point1.position.x = 1.3;
       point1.position.y = 0;
       point1.position.z = -15;


        this.object.add(point1);

       // white spotlight shining from the side, casting a shadow

       this.spotLight1 = new THREE.SpotLight( 0x404040, 3, 500, Math.PI/2, 2 );
       this.spotLight1.position.set( 1.3, 3 , -6);
       this.spotLight1.target = point1;
       this.spotLight1.angle = 1;
       this.spotLight1.penumbra = .5;
//       spotLight1.intensity = 0; // old 3

        this.object.add( this.spotLight1 );


        // PHARE 2 //
        var geometry = new THREE.SphereGeometry(0.001);

        var material = new THREE.MeshBasicMaterial({
            alphaTest : 0,
            transparent:true,
            side : THREE.DoubleSide
        });

        var point2 = new THREE.Mesh(geometry, material);

        point2.position.x = 2.3;
        point2.position.y = 0;
        point2.position.z = -15;


         this.object.add(point2);

        // white spotlight shining from the side, casting a shadow

        this.spotLight2 = new THREE.SpotLight( 0x404040, 3, 500, Math.PI/2, 2 );
        this.spotLight2.position.set( 2.3, 3 , -6);
        this.spotLight2.target = point2;
        this.spotLight2.angle = 1;
        this.spotLight2.penumbra = .5;
//        spotLight2.intensity = 0; // old 3

        this.object.add( this.spotLight2 );

    }

    initMaterial(){


    var managerEnd = STORAGE.managerEnd;

      var mtlLoaderGlock = new THREE.MTLLoader();
       mtlLoaderGlock.setPath( 'pump/' );
       mtlLoaderGlock.load( 'pump.mtl', ( materials ) => {
            materials.preload();
            var objLoader = new THREE.OBJLoader();
            objLoader.setMaterials( materials );
            objLoader.setPath( 'pump/' );
            objLoader.load( 'pump.obj',  ( object ) => {
                object.position.z = -0.75;
                object.position.x = 1.8;
                object.position.y = -0.23;
                object.scale.set(0.05, 0.05, 0.05)
                object.rotation.z = - Math.PI/2;
                object.rotation.x = - Math.PI/3;

                  this.object.add(object);

                  managerEnd();

            });
       });


    }

    initSounds(){



        //Create an object for the sound to play from
        var sphere = new THREE.SphereGeometry( 0.001, 32, 16 );
        var material = new THREE.MeshPhongMaterial( { color: 0xff2200, visible : true} );
        this.motor = new THREE.Mesh( sphere, material );
        this.object.add( this.motor );
        this.motor.position.x =  1.6;
        this.motor.position.z = - 3.5;
        this.motor.position.y = -0.2;

        // create a global audio source
        this.ambiantDown = new THREE.PositionalAudio( STORAGE.listener );

        //Load a sound and set it as the Audio object's buffer
        STORAGE.audioLoader.load( 'sounds/down.wav', ( buffer ) => {
            this.ambiantDown.setBuffer( buffer );
            this.ambiantDown.setRefDistance( 0.05 );
            this.ambiantDown.setVolume(1.2);
        });

                //Finally add the sound to the mesh
                this.motor.add( this.ambiantDown );

        // create a global audio source
        this.ambiantDownStill = new THREE.PositionalAudio( STORAGE.listener );

        //Load a sound and set it as the Audio object's buffer
        STORAGE.audioLoader.load( 'sounds/downStill.wav', ( buffer ) => {
            this.ambiantDownStill.setBuffer( buffer );
            this.ambiantDownStill.setLoop(true);
            this.ambiantDownStill.setRefDistance( 0.05 );
            this.ambiantDownStill.setVolume(1.2);
            this.ambiantDownStill.play();
        });


                //Finally add the sound to the mesh
                this.motor.add( this.ambiantDownStill );

        // create a global audio source
        this.ambiantUp = new THREE.PositionalAudio( STORAGE.listener );

        //Load a sound and set it as the Audio object's buffer
        STORAGE.audioLoader.load( 'sounds/up.wav', ( buffer ) => {
            this.ambiantUp.setBuffer( buffer );
            this.ambiantUp.setRefDistance( 0.05 );
            this.ambiantUp.setVolume(1.3);
        });


                //Finally add the sound to the mesh
                this.motor.add( this.ambiantUp );

        // create a global audio source
        this.ambiantUpStill = new THREE.PositionalAudio( STORAGE.listener );

        //Load a sound and set it as the Audio object's buffer
        STORAGE.audioLoader.load( 'sounds/upStill.wav', ( buffer ) => {
            this.ambiantUpStill.setBuffer( buffer );
            this.ambiantUpStill.setRefDistance( 0.05 );
            this.ambiantUpStill.setLoop(true);
            this.ambiantUpStill.setVolume(1.3);
        });


        //Finally add the sound to the mesh
        this.motor.add( this.ambiantUpStill );


        let musics = ['tuning.wav', 'music.mp3', 'music1.mp3', 'music2.mp3', 'music3.mp3', 'music4.mp3'];

        //Create an object for the sound to play from
        var sphere = new THREE.SphereGeometry( 0.001, 32, 16 );
        var material = new THREE.MeshPhongMaterial( { color: 0xff2200, visible : true} );
        this.mesh = new THREE.Mesh( sphere, material );
        this.object.add( this.mesh );
        this.mesh.position.x =  1.6;
        this.mesh.position.z = - 1.5;
        this.mesh.position.y = -0.2;

        for(let i = 0; i < musics.length; i++){


            let music = new THREE.PositionalAudio( STORAGE.listener );

            //Load a sound and set it as the Audio object's buffer
            STORAGE.audioLoader.load( 'sounds/' + musics[i], ( buffer ) => {
                music.setBuffer( buffer );
                music.setLoop(true);
                music.setRefDistance( 0.05 );
                music.setVolume(1.7);

                //Finally add the sound to the mesh
                this.mesh.add( music );

                if(i == 1) music.play();

                this.musicsPlayables.push(music);
                this.musicsLoaded += 1;

            });


        }

    }

    update(){

        if(this.vitess.state != 'none') STORAGE.vitess = this.vitess.value;
        this.car.rotation.x = this.axe.value;

    }
}

export default Car
