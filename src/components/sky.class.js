import * as THREE from 'three'
import SkyL from '../libs/Sky.js'

class Sky {

    constructor(options) {

        this.object = new THREE.Group();

        this.value = 0;
        this.angle = {
            value: 0.001,
            min : 0.001,
            max : 0.021
        };
        this.initMaterial();
        this.init();


    }

    init() {


        this.sky = new THREE.Sky();
        this.object.add(this.sky.mesh);

        this.tween = TweenMax.fromTo(this.angle, 60, {value: this.angle.min},{value: this.angle.max});
        this.tween.pause();

        this.sunSphere = new THREE.Mesh(
        	new THREE.SphereBufferGeometry( 1, 16, 8 ),
        	new THREE.MeshBasicMaterial( { color: 0xffffff } )
        );
        this.sunSphere.position.y = - 70;
        this.sunSphere.visible = false;
        this.object.add( this.sunSphere );

        this.effectController  = {
        	turbidity: 10,
        	rayleigh: 2,
        	mieCoefficient: 0.005,
        	mieDirectionalG: 0.8,
        	luminance: 1,
        	inclination: 0.49, // elevation / inclination
        	azimuth: 0.25, // Facing front,
        	sun: true
        };
        this.distance = 400000;



        var uniforms = this.sky.uniforms;
        uniforms.turbidity.value = this.effectController.turbidity;
        uniforms.rayleigh.value = this.effectController.rayleigh;
        uniforms.luminance.value = this.effectController.luminance;
        uniforms.mieCoefficient.value = this.effectController.mieCoefficient;
        uniforms.mieDirectionalG.value = this.effectController.mieDirectionalG;

        var theta = Math.PI * ( this.effectController.inclination - 0.5 );
        var phi = 2 * Math.PI * ( this.effectController.azimuth - 0.5 );
        this.sunSphere.position.x = this.distance * Math.cos( phi );
        this.sunSphere.position.y = this.distance * Math.sin( phi ) * Math.sin( theta );
        this.sunSphere.position.z = this.distance * Math.sin( phi ) * Math.cos( theta );
        this.sunSphere.visible = this.effectController.sun;
        this.sky.uniforms.sunPosition.value.copy( this.sunSphere.position );



    }

    initMaterial(){



    }

    update(){

        if(this.tween._active == false){
            this.tween.play();
        }

        var theta = Math.PI * ( this.effectController.inclination - 0.5  + this.angle.value);
        var phi = 2 * Math.PI * ( this.effectController.azimuth - 0.5 );
        this.sunSphere.position.x = this.distance * Math.cos( phi );
        this.sunSphere.position.y = this.distance * Math.sin( phi ) * Math.sin( theta );
        this.sunSphere.position.z = this.distance * Math.sin( phi ) * Math.cos( theta );
        this.sky.uniforms.sunPosition.value.copy( this.sunSphere.position );

    }
}

export default Sky
