module.exports = function(grunt) {
    grunt.initConfig({
        browserify: {
            dist: {
                options: {
                    transform: [
                        ["babelify", {
                        loose: "all"
                        }],
                        "glslify"
                    ]
                },
                files: {
                    "../build/javascript/bundle.js": ["./index.js"]
                }
            }
        },
        watch: {
            scripts: {
                files: [
                    "./*.js",
                    "./components/*.js",
                    "./sass/*.scss",
                    "./shaders/*.vert",
                    "./shaders/*.frag"
                ],
                tasks: ["browserify", "notify:watch"]
            }
        },
        notify: {
            watch: {
                options: {
                    title: 'Creative Project',
                    message: 'SASS and Browserfiy finished running'
                }
            },
            build: {
                options: {
                    title: 'Creative Project',
                    message: 'Build Successful !'
                }
            }
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        '../build/stylesheets/*.css',
                        '../build/javascript/*.js',
                        '../build/*.html'
                    ]
                },
                options: {
                    watchTask: true,
                    server: '../build'
                }
            }
        }
    });

    grunt.loadNpmTasks("grunt-browserify");
    grunt.loadNpmTasks("grunt-contrib-watch");
//    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-browser-sync');

    grunt.registerTask("default", ["browserSync", "watch"]);
    grunt.registerTask("build", ["browserify", "notify:build"]);
};
